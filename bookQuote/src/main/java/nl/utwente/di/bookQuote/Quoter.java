package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    private final HashMap<String, Double> isbnList;

    public Quoter() {
        isbnList = new HashMap<>();
        isbnList.put("1", 10.0);
        isbnList.put("2", 45.0);
        isbnList.put("3", 20.0);
        isbnList.put("4", 35.0);
        isbnList.put("5", 50.0);
    }

    public double getBookPrice(String isbn) {
        return isbnList.getOrDefault(isbn, 0.0);
    }



}
