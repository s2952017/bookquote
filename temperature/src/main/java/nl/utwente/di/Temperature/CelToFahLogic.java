package nl.utwente.di.Temperature;
public class CelToFahLogic {

    public CelToFahLogic() {
    }

    public double calcFahrenheit(String celsius) {
        try {
            return (Integer.parseInt(celsius) * 9.0d / 5.0d) + 32;
        } catch (NumberFormatException e) {
            return -100d;
        }
    }

}
